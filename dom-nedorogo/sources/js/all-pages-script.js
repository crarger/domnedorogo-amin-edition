//скрипт для закрытия меню бутстрап при клике вне него
$(document).bind("mouseup touchend", function (e) {
    var container = $('.navbar-collapse');

    if (!container.is(e.target)
        && container.has(e.target).length === 0) {
        container.collapse('hide');
    }
});


//progressbar owl (первый экран на главной)
$(document).ready(function () {
    //Init the carousel
    initSlider();

    function initSlider() {
        $("#first-slider").owlCarousel({
            loop: true,
            margin: 0,
            stagePadding: 0,
            nav: false,
            autoplaySpeed: 4000,
            dotsSpeed: 4000,
            navText: "",
            dots: false,
            paginationNumbers: true,
            items: 1,
            lazyLoad: true,
            autoplay: true,
            animateOut: 'fadeOut',
            animateIn: 'fadeIn',
            autoplayTimeout: 7000,
            mouseDrag: false,
            touchDrag: true,
            pullDrag: false,
            freeDrag: false,
            onInitialized: startProgressBar,
            onTranslate: resetProgressBar,
            onTranslated: startProgressBar,
            navigation: true,
            navigationText: ['&lsaquo;', '&rsaquo;'],
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: true,
            afterInit: makePages,
            afterUpdate: makePages
        });

    }

    function startProgressBar() {
        // apply keyframe animation
        $('.slide-progress').css({
            'width': '100%',
            'transition': 'width 6000ms'
        });
    }

    function makePages() {
        $.each(this.owl.userItems, function (i) {
            $('.owl-controls .owl-page').eq(i)
                .text($(this).find('p').text());
        });
    }

    function resetProgressBar() {
        $('.slide-progress').css({
            'width': 0,
            'transition': 'width 0s'
        });
    }
});

//инициализация owl для screen-two на главной
$("#start-page-slider-two").owlCarousel({
    loop: true,
    margin: 0,
    stagePadding: 0,
    nav: false,
    autoplaySpeed: 4000,
    dotsSpeed: 4000,
    navText: "",
    dots: true,
    paginationNumbers: true,
    items: 1,
    lazyLoad: true,
    autoplay: true,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    autoplayTimeout: 15000,
    mouseDrag: false,
    touchDrag: true,
    pullDrag: false,
    freeDrag: false
});


//Делегируем события кнопок next prev для screen-two на главной
var owlStartPageSliderTwo = $("#start-page-slider-two");
//$(".next") - находим нашу кнопку
$(".btn-next").click(function () {
    owlStartPageSliderTwo.trigger("next.owl.carousel");
});
$(".btn-prev").click(function () {
    owlStartPageSliderTwo.trigger("prev.owl.carousel");
});


//инициализация owl для блока партнеров
$("#start-page-partners").owlCarousel({
    loop: true,
    margin: 0,
    stagePadding: 0,
    nav: false,
    autoplaySpeed: 4000,
    dotsSpeed: 4000,
    navText: "",
    dots: true,
    paginationNumbers: true,
    lazyLoad: true,
    autoplay: true,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    autoplayTimeout: 6000,
    mouseDrag: false,
    touchDrag: true,
    pullDrag: false,
    freeDrag: false,
    responsive: {
        320: {items: 1},
        430: {items: 2},
        600: {items: 3},
        768: {items: 4}
    }
});

//инициализация owl на странице производства
$("#photo-from-factory").owlCarousel({
    loop: true,
    margin: 30,
    stagePadding: 0,
    nav: false,
    autoplaySpeed: 4000,
    dotsSpeed: 200,
    navText: "",
    dots: true,
    paginationNumbers: true,
    lazyLoad: true,
    autoplay: true,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    autoplayTimeout: 6000,
    mouseDrag: false,
    touchDrag: true,
    pullDrag: false,
    freeDrag: false,
    responsive: {
        320: {
            items: 1
        },
        560: {
            items: 2
        },
        768: {
            items: 3
        },
        1080: {
            items: 4
        },
        1290: {
            items: 5
        }
    }
});


//скрипты для слайдеров в project-detail
$('.work .slider-project').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.work .slider-project-nav',
    arrows: false,
    dots: false,
    fade: true
});
$('.work .slider-project-nav').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    asNavFor: '.work .slider-project',
    arrows: true,
    dots: false,
    focusOnSelect: true,
    infinite: false
});


//кнопки для переключения nav-tabs
$('.nav-btns .btn-next').click(function(){
    $('.block-desctop > .navs > .nav-tabs > .active').next('li').find('a').trigger('click');
});

$('.nav-btns .btn-prev').click(function(){
    $('.block-desctop > .navs > .nav-tabs > .active').prev('li').find('a').trigger('click');
});


// анимация перехода по якорям
$("body").on("click","a[href^='#jack']", function (event) {
    event.preventDefault();
    var id  = $(this).attr('href'),
        top = $(id).offset().top;
    $('body,html').animate({scrollTop: top-0}, 1800);
});



// открытие/закрытие блока калькулятора при клике на кнопки
var
    content = $('#calc'),
    footer = $('.footer');

$('#show-discount').on('click', function (e) {
    e.preventDefault();
    content.slideDown();
    footer.toggle();
});


$('#hide-discount').on('click', function (e) {
    e.preventDefault();
    content.slideUp();
    footer.show('fast');
});


//иницавлизация всплывающих подсказок
$(function () {
    $('[data-toggle="tooltip"]').tooltip();
});

//Маска телефона
$('input[name="num-phone"]').inputmask('+7 999 999-99-99', {placeholder: '*'}).attr('type', 'tel');