﻿//инициализация owl
$(".owl-carousel").owlCarousel({
    loop: true,
    margin: 30,
    stagePadding: 0,
    nav: false,
    autoplaySpeed: 4000,
    dotsSpeed: 200,
    navText: "",
    dots: true,
    paginationNumbers: true,
    lazyLoad: true,
    autoplay: true,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    autoplayTimeout: 6000,
    mouseDrag: false,
    touchDrag: true,
    pullDrag: false,
    freeDrag: false,
    responsive: {
        320: {
            items: 1
        },
        560: {
            items: 2
        },
        768: {
            items: 3
        },
        1080: {
            items: 4
        },
        1290: {
            items: 5
        }
    }
});