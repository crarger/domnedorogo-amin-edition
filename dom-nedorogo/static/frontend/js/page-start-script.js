﻿// // открытие/закрытие блока калькулятора при клике на кнопки
// var
//     content = $('#calc'),
//     footer = $('.footer');
//
// $('#show-discount').on('click', function (e) {
//     e.preventDefault();
//     content.slideDown();
//     footer.toggle();
// });
//
//
// $('#hide-discount').on('click', function (e) {
//     e.preventDefault();
//     content.slideUp();
//     footer.show('fast');
// });


if ($(window).width() > 1280) {

    //Добавление класса при спуске до опредененной позиции
    $(document).on("scroll", function () {


        if (jQuery(window).scrollTop() + jQuery(window).height() >= jQuery(document).height() + 999999) {
            content.slideDown();
            footer.css("opacity", "0");
            footer.hide();

        }
        if ($(document).scrollTop() > 1350) {
            $(".footer").addClass("light-footer");
        }
        else {
            $(".footer").removeClass("light-footer");
        }
        if ($(document).scrollTop() > 1850) {
            $(".header").addClass("navbar-setting").removeClass("nav-default");
        }
        else {
            $(".header").removeClass("navbar-setting").addClass("nav-default");
        }
        if ($(document).scrollTop() > 3100) {
            $(".footer").removeClass("light-footer").addClass("dark-footer");
        }
        else {
            $(".footer").removeClass("dark-footer");
        }
        if ($(document).scrollTop() > 500) {
            $(".left-menu").addClass("gray-color");
        }
        else
            $(".left-menu").removeClass("gray-color");
        if ($(document).scrollTop() > 1700) {
            $(".left-menu").addClass("dark-color").removeClass("gray-color");
        }
        else {
            $(".left-menu").removeClass("dark-color");
        }
        if ($(document).scrollTop() > 70) {
            $(".footer").css("opacity", "1");
            $(".footer").css("z-index", "5");
        } else {
            $(".footer").css("opacity", "0");
        }
        if ($(document).scrollTop() < 10) {
            $(".footer").css("z-index", "-500");
        }
    });

}

try {
//подсвечивание активного пункта левого меню
var menu_selector = ".left-menu"; // Переменная должна содержать название класса или идентификатора, обертки нашего меню.

function onScroll() {
    var scroll_top = $(document).scrollTop();
    $(menu_selector + " li").each(function () {
        var hash = $(this).children('a').attr("href");
        var target = $(hash);
        if (target.position().top - 500 <= scroll_top && target.position().top + target.outerHeight() > scroll_top) {
            $(menu_selector + " li.active").removeClass("active");
            $(this).addClass("active");
        } else {
            $(this).removeClass("active");
        }
    });
}

$(document).ready(function () {
    $(document).on("scroll", onScroll);
    $("a[href^=#]").click(function (e) {
        e.preventDefault();
        $(document).off("scroll");
        $(menu_selector + " li.active").removeClass("active");
        $(this).addClass("active");
        var hash = $(this).children('a').attr("href");
        var target = $(hash);
        $("html, body").animate({
            scrollTop: target.offset().top
        }, 500 , function () {
            window.location.hash = hash;
            $(document).on("scroll", onScroll);
        });
    });
});
} catch (err) {
    /* Исключение */
}

//настройка набегания цифр
let marker = true;
try {
    $(document).on("scroll", function () {
        let block1 = $(".screen-five").offset().top;
        let block3 = $(window).height();
        if ($(document).scrollTop() > block1 - block3 && marker) {
            $('.timer').countTo();
            marker = false;
        }
    });
} catch (err) {
    /* Исключение */
}


$("#slider-action").owlCarousel({
    loop: true,
    margin: 0,
    stagePadding: 0,
    nav: false,
    autoplaySpeed: 4000,
    dotsSpeed: 4000,
    navText: "",
    dots: true,
    paginationNumbers: true,
    items: 1,
    lazyLoad: true,
    autoplay: true,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    autoplayTimeout: 15000,
    mouseDrag: false,
    touchDrag: true,
    pullDrag: false,
    freeDrag: false
});