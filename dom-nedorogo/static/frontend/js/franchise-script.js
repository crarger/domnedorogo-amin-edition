try {
    $(document).on("scroll", function () {
        var block1 = $(".figures-container").offset().top,
            block3 = $(window).height();
        if ($(document).scrollTop() > block1 - block3) {
            $('.timer').countTo();
        }
    });
} catch (err) {
    /* Исключение */
}

$(document).on("scroll", function () {
    if ($(document).scrollTop() > 500) {
        $(".btn-scroll-to-top").css({'display' : 'block','opacity' : '1'});
    }
    else $(".btn-scroll-to-top").css({'opacity' : '0', 'display': 'none'});
});