from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView

from .views import *

urlpatterns = [
    url(r'^$', home),
    url(r'^about/$', about),
    url(r'^contacts/$', contacts),
    url(r'^works/$', works),
    url(r'^work/(?P<id>\d+)/$', work),
    url(r'^work/$', work),
    url(r'^intermediate-page/$', ProPage),
    url(r'^projects/$', projects),
    url(r'^projects/(?P<sort>\d+)/$', projects),
    url(r'^project/(?P<id>\d+)/$', project_detail),
    url(r'^question/(?P<id>\d+)/$', question),
    url(r'^question/$', question),
    url(r'^technology/(?P<id>\d+)/$', technology),
    url(r'^technology/$', technology),
    url(r'^service/(?P<id>\d+)/$', service),
    url(r'^service/$', service),
    url(r'^franchise/$', franchise),
    url(r'^doc/$', doc),
    url(r'^company/$', company),

    url(r'^download-present/$', DownloadPresent),
    url(r'^save_user/(?P<city>([^/]+))/(?P<ip>([^/]+))/$', SaveUser),
    #url(r'^city_user/(?P<ip>([^/]+))/$', GetCityUser),
    url(r'^get_equ/(?P<equ_id>\d+)/$', GetEquipments),
    url(r'^franchise-request/$', FranchiseRequest),
    url(r'^home-request/$', HomeRequest),
    url(r'^get_comment/(?P<id>\d+)/$', GetComment),
    url(r'^company_request/$', CompanyRequest),
    url(r'^project_request/$', project_send_mail),

    url(r'^robots\.txt$', TemplateView.as_view(template_name='frontend/robots.txt.html'), name='robots'),

	url(r'^404/$', page_not_found),

    url(r'^canada/$', redirect_urls),
    url(r'^project/$', redirect_project),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
