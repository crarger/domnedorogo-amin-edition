﻿from django.db import models

# Create your models here.

# Слайдеры
class MainSlider(models.Model):
    id = models.AutoField(primary_key = True)
    slide = models.ImageField(verbose_name = u'Изображение слайда')
    text = models.CharField(max_length = 255, verbose_name = u'Текст слайда')

    def __str__(self):
        return '%s' % (self.text)
        
    class Meta:
    	verbose_name_plural = 'Первый слайдер'

class BottomSlider(models.Model):
    id = models.AutoField(primary_key = True)
    slide = models.ImageField(verbose_name = u'Изображение слайда')
    text = models.TextField(verbose_name = u'Текст слайда')
    srv = models.ForeignKey('Service')

    def __str__(self):
        return '%s' % (self.text)
        
    class Meta:
    	verbose_name_plural = 'Второй слайдер'

# Таблица пунктов меню
class Menu(models.Model):
    id = models.AutoField(primary_key = True)
    submenu = models.ManyToManyField('Submenu', null = True, blank = True, verbose_name = u'Подменю')
    name_item = models.CharField(max_length = 32, verbose_name = u'Имя пункта меню')
    link_item = models.CharField(max_length = 32, null = True, blank = True, verbose_name = u'Cсылка пункта меню')
    image = models.FileField(verbose_name = u'Изображение пункта меню')
    image_hover = models.FileField(verbose_name = u'Изображение при наведении пункта меню')
    id_block = models.CharField(max_length = 32, verbose_name = u'ID пункта меню')
    dropdown = models.BooleanField()
    
    def __str__(self):
        return '%s (%s)' % (self.name_item, self.link_item)
        
    class Meta:
    	verbose_name_plural = 'Пункты главного меню'

# Таблица подпунктов меню
class Submenu(models.Model):
    id = models.AutoField(primary_key = True)
    name_item = models.CharField(max_length = 255, verbose_name = u'Имя подменю')
    link_item = models.CharField(max_length = 255, verbose_name = u'Ссылка подменю')
    
    def __str__(self):
    	return '%s (%s)' % (self.name_item, self.link_item)
    	
    class Meta:    	
    	verbose_name_plural = 'Подпункты главного меню'
    		
# Таблица категорий часто задаваемых вопросов
class CategoryQuest(models.Model):
	id = models.AutoField(primary_key = True)
	quest = models.ManyToManyField('Quest', null = True, blank = True, verbose_name = u'Часто задаваемые вопросы')
	name = models.CharField(max_length = 32, verbose_name = u'Имя категории')
	
	def __str__(self):
		return self.name
		
	class Meta:
		verbose_name_plural = 'Категории часто задаваемых вопросов'
		
# Таблица часто задаваемых вопросов
class Quest(models.Model):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 65, verbose_name = u'Имя часто задаваемого вопроса')
    article = models.ForeignKey('ArticleQuest', verbose_name = u'Статья вопроса')
    tags = models.ManyToManyField('Tags', null = True, blank = True, verbose_name = u'Теги')
    techn = models.ManyToManyField('Technology', null = True, blank = True, verbose_name = u'Технологии', related_name = 'techn')
    service = models.ManyToManyField('Service', null = True, blank = True, verbose_name = 'Услуги связанные с этим вопросом')
    images = models.ForeignKey('ImageArticleQuest', verbose_name = u'Изображения статьи')

    def __str__(self):
        return self.name
		
    class Meta:
        verbose_name_plural = 'Часто задаваемые вопросы'

# Изображения статей
class ImageArticleQuest(models.Model):
    id = models.AutoField(primary_key = True)
    image = models.ImageField(verbose_name = 'Изображение которое вы увидите в статье')

# Таблица статей часто задаваемых вопросов
class ArticleQuest(models.Model):
    id = models.AutoField(primary_key = True)
    image = models.ImageField(verbose_name = 'Изображение статьи')
    article = models.TextField(max_length = 9001)
        
    def __str__(self):
        return self.article[0:10] + '...'
        
    class Meta:
        verbose_name_plural = 'Статьи часто задаваемых вопросов'
        
# Таблица категорий технологий ввиду букв
class CategoryTechnology(models.Model):
	id = models.AutoField(primary_key = True)
	technology = models.ManyToManyField('Technology', null = True, blank = True, verbose_name = u'Технологии')
	name = models.CharField(max_length = 1, verbose_name = u'Имя категории (например "П")')
	
	def __str__(self):
		return 'Категория (%s)' % (self.name)
		
	class Meta:
		verbose_name_plural = 'Категории технологий'
		
# Таблица технологий
class Technology(models.Model):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 65, verbose_name = u'Имя технологии')
    article = models.ForeignKey('ArticleTechnology')
    tags = models.ManyToManyField('Tags', null = True, blank = True, verbose_name = u'Теги')
    quest = models.ManyToManyField('Quest', null = True, blank = True, verbose_name = u'Часто задаваемые вопросы')
    project = models.ManyToManyField('Project', null = True, blank = True, verbose_name = 'Проекты с использованием данной технологии', related_name = 'proj')
    images = models.ManyToManyField('ImageArticleTechnology', verbose_name = u'Изображения статьи')
    
    def __str__(self):
        return self.name
		
    class Meta:
        verbose_name_plural = 'Технологии'

# Изображения статей
class ImageArticleTechnology(models.Model):
    id = models.AutoField(primary_key = True)
    image = models.ImageField(verbose_name = 'Изображение которое вы увидите в статье')

# Статьи технологий
class ArticleTechnology(models.Model):
    id = models.AutoField(primary_key = True)
    image = models.ImageField(verbose_name = 'Изображение статьи')
    article_top = models.TextField(max_length = 700, verbose_name = u'Текст статьи сверху')
    article = models.TextField(max_length = 9000, verbose_name = u'Текст статьи')

    def __str__(self):
        return self.article[0:32] + '...'
    
    class Meta:
        verbose_name_plural = 'Статьи технологий'

        
# Таблица услуг
class Service(models.Model):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 255, verbose_name = u'Имя услуги')
    article = models.ForeignKey('ArticleService', verbose_name = u'Статья услуги')
    text_slider = models.TextField(max_length = 1000, null = True, blank = True)
    on_images = models.IntegerField(verbose_name = u'Блок картинкок')

    on_advantages = models.IntegerField(verbose_name = u'Блок преимуществ')
    title_advantages = models.CharField(max_length = 9000, verbose_name = 'Заголовок блока преимуществ')
    advantages = models.ManyToManyField('AdvatagesIcon', verbose_name = 'Преимущества')

    on_images_tabs = models.IntegerField(verbose_name = u'Блок изображений с табами')
    title_image_block = models.CharField(max_length = 255, verbose_name = u'Заголовок блока с картинками')
    block_images = models.ManyToManyField('ImageService', verbose_name = u'Выберите изображения которые будут выводиться')
    imageSlide = models.ImageField(verbose_name = 'Изображение слайда')

    def __str__(self):
        return self.name
    
    class Meta:
    	verbose_name_plural = 'Услуги'

# Таблица с изображениями услуги (добавляются в блок)
class ImageService(models.Model):
    id = models.AutoField(primary_key = True)
    image = models.ImageField(verbose_name = u'Изображение для блока')
    
    def __str__(self):
        return self.image.url

# Таблица статей услуг
class ArticleService(models.Model):
    id = models.AutoField(primary_key = True)
    article = models.TextField(max_length = 99000)
    
    def __str__(self):
        return  self.article[0:32] + '...'
    
    class Meta:    	
    	verbose_name_plural = 'Статьи услуг'

# Года
class Years(models.Model):
    id = models.AutoField(primary_key = True)
    year = models.IntegerField(verbose_name = u'Год')
    counters = models.ManyToManyField('Counter', null = True, blank = True, verbose_name = u'Счётчики')
    class_counter = models.CharField(max_length = 255, null = True, blank = True, verbose_name = u'Класс для счётчика')
    status = models.CharField(max_length = 255, null = True, blank = True, verbose_name = u'Статус')

    def __str__(self):
        return '%s - %s' % (self.year, self.status)

    class Meta:
        verbose_name_plural = 'Года'
        
# Имена счётчиков на сайте
class NameCounter(models.Model):
    id = models.AutoField(primary_key = True)
    top_text = models.CharField(max_length = 32)
    bottom_text = models.CharField(max_length = 32)

    def __str__(self):
        return '%s - %s' % (self.top_text, self.bottom_text)
    
    class Meta:    	
    	verbose_name_plural = 'Наименование счётчиков на сайте'
    
# Таблица со счётчиками
class Counter(models.Model):
    id = models.AutoField(primary_key = True)
    counter = models.ForeignKey(NameCounter)
    count = models.IntegerField()
    status = models.IntegerField()
    
    def __str__(self):
        return '%s - %s - %s' % (self.counter, self.count, self.status)

    class Meta:    	
    	verbose_name_plural = 'Счётчики сайта'
        
# Таблица городов
class Cities(models.Model):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 32)
    phone = models.ManyToManyField('HeaderContacts', related_name = 'phoned', verbose_name = u'Телефоны города')
    
    def __str__(self):
        return self.name
    
    class Meta:    	
    	verbose_name_plural = 'Список городов'
        
# Таблица тегов
class Tags(models.Model):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 32)
    
    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name_plural = 'Теги'
        
# Таблица нащи работы
class OWorks(models.Model):
    id = models.AutoField(primary_key = True)
    image = models.ImageField()
    city = models.ForeignKey('Cities')
    name = models.CharField(max_length = 32, verbose_name = u'Имя работы')
    mouth = models.CharField(max_length = 32, verbose_name = u'Месяц')
    year = models.CharField(max_length = 32, verbose_name = 'Год')

    status = models.IntegerField(verbose_name = 'Статус работы')

    all = models.BooleanField(verbose_name = 'Другие')
    
    description = models.CharField(max_length = 255)
    
    images = models.ManyToManyField('ImageWork', null = True, blank = True)

    project = models.ForeignKey('Project', null = True, blank = True)
    
    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name_plural = u'Список работ'
        
class ImageWork(models.Model):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 32)
    image = models.ImageField()
        
    def __str__(self):
        return '%s (<img src="%s">)' % (self.name, self.image)
    
    class Meta:
        verbose_name_plural = u'Список изображений (Наши работы)'
        
# Социальные сети
class SocialNetwork(models.Model):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 32)
    class_icon = models.CharField(max_length = 16)
    
    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name_plural = u'Список иконок социальных сетей'
    
class ContactsSocialNetwork(models.Model):
    id = models.AutoField(primary_key = True)
    link = models.CharField(max_length = 255)
    icon = models.ForeignKey('SocialNetwork')
    
    def __str__(self):
        return self.link
    
    class Meta:
        verbose_name_plural = u'Список cоциальных сетей'

# Контакты
class Contacts(models.Model):
    id = models.AutoField(primary_key = True)
    region = models.CharField(max_length = 32)
    city = models.ForeignKey('Cities')
    address = models.CharField(max_length = 32)
    phone = models.CharField(max_length = 40)
    email = models.EmailField()
    zoom = models.IntegerField(verbose_name = 'Масштаб ', null = True)
    lat = models.CharField(max_length = 32, verbose_name = 'Ширина ', null = True)
    lng = models.CharField(max_length = 32, verbose_name = 'Долгота ', null = True)
    
    def __str__(self):
        return '%s - %s - %s - %s - %s' % (self.region, self.city, self.address, self.phone, self.email)
    
    class Meta:
        verbose_name_plural = u'Контакты'
        

# Контакты в шапке
class HeaderContacts(models.Model):
    id = models.AutoField(primary_key = True)
    link = models.CharField(max_length = 32)
    phone = models.CharField(max_length = 32)

# Проекты
class Project(models.Model):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 16, verbose_name = 'Имя')
    description = models.CharField(max_length = 255, verbose_name = 'Описание')
    amount = models.CharField(max_length = 16, verbose_name = 'Цена')
    main_image = models.ImageField(verbose_name = 'Основное изображение проекта')
    image = models.ManyToManyField('ImageProject', null = True, blank = True, verbose_name = 'Изображния проекта')
    equ = models.ManyToManyField('Equipment', verbose_name = 'Комплектации')
    data = models.ForeignKey('ProjectData', verbose_name = 'Данные проекта')
    layout = models.ForeignKey('ProjectLayout', verbose_name = 'Планировка', null = True)
    tags = models.ManyToManyField('Tags', null = True, blank = True, verbose_name = 'Теги')
    techn = models.ManyToManyField('Technology', null = True, blank = True, related_name = 'pt')

    def __str__(self):
        return 'Наименование %s | Описание %s | Цена %s' % (self.name, self.description, self.amount)
    
    class Meta:
        verbose_name_plural = u'Проекты'
    
    
class ImageProject(models.Model):
    id = models.AutoField(primary_key = True)
    image = models.ImageField(verbose_name = 'Изображение')
    
    def __str__(self):
        return 'Изоббражение %s' % (self.image)
    
    class Meta:
        verbose_name_plural = u'Изображения проектов'
    
class Equipment(models.Model):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 32, verbose_name = 'Имя')
    equ = models.TextField(max_length = 9000, verbose_name = 'Комплектация')
    amount = models.IntegerField(verbose_name = 'Цена комплектации')
    active = models.CharField(max_length = 32, verbose_name = 'Активная комплектация')
    
    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name_plural = u'Комплектации'
    
class ProjectData(models.Model):
    id = models.AutoField(primary_key = True)

    total_area = models.IntegerField(verbose_name = 'Общая площадь')
    living_space = models.CharField(max_length = 255, verbose_name = 'Жилая площадь')

    number_floors = models.IntegerField(verbose_name = 'Количество этажей')
    number_rooms = models.CharField(max_length = 10, verbose_name = 'Количество комнат')

    number_bathrooms = models.IntegerField(verbose_name = 'Количество санузлов')
    number_terraces = models.IntegerField(verbose_name = 'Количество террас')

    height_one = models.CharField(max_length = 300, verbose_name = 'Высота первого этажа')
    height_two = models.CharField(max_length = 300, verbose_name = 'Высота второго этажа')

    width = models.CharField(max_length = 322, verbose_name = 'Ширина дома')
    height = models.CharField(max_length = 322, verbose_name = 'Высота дома')
    
    def __str__(self):
        return 'Чистая площадь м2: %s' %  (self.total_area)
    
    class Meta:
        verbose_name_plural = u'Данные проекта'
    
class ProjectLayout(models.Model):
    id = models.AutoField(primary_key = True)
    garage_area = models.IntegerField(verbose_name = 'Площадь гаража')
    
    def __str__(self):
        return 'Гараж м2: %s' % (str(self.garage_area))
    
    class Meta:
        verbose_name_plural = u'Планировка'
        
# Презентация
class Present(models.Model):
    id = models.AutoField(primary_key = True)
    file_present = models.FileField()

# Пользователи (анонимы)
class Anonimus(models.Model):
    id = models.AutoField(primary_key = True)
    ip_addr = models.CharField(max_length = 32, unique = True, verbose_name = u'IP адрес')
    city = models.CharField(max_length = 32, verbose_name = u'Город')

    class Meta:
        verbose_name_plural = u'Анонимные пользователи'

# Преимущества
class AdvatagesIcon(models.Model):
    id = models.AutoField(primary_key = True)
    icon = models.ImageField()
    caption = models.CharField(max_length = 64)
    description = models.CharField(max_length = 255)


# Настройки сайта
class Settings(models.Model):
    id = models.AutoField(primary_key = True)
    stages_image = models.ImageField(blank = True, null = True)
    #stages = models.ManyToManyField('Stages', blank = True, null = True)

# Этапы
class Stages(models.Model):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 64)
    description = models.CharField(max_length = 255)

# Партнёры
class Partners(models.Model):
    id = models.AutoField(primary_key = True)
    image = models.ImageField()
    link = models.CharField(max_length = 255)

    def __str__(self):
        return 'Партнёр: %s' % (str(self.link))
    
    class Meta:
        verbose_name_plural = u'Партнёры'

# О компании
class About(models.Model):
    id = models.AutoField(primary_key = True)
    top_text = models.TextField(max_length = 900000)
    background = models.CharField(max_length = 32)
    color = models.CharField(max_length = 32)
    image_text = models.TextField(max_length = 900000)
    image_founder = models.ImageField()
    fullname_founder = models.CharField(max_length = 500)
    description_founder = models.CharField(max_length = 800)
    about_founder = models.TextField(max_length = 900000)
    franchise_text = models.TextField(max_length = 900000)
    franchise_description = models.TextField(max_length = 900000)

# Слайдер 1
class SlideTop(models.Model):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 112)
    image = models.ImageField()

# Фриншиза отзывы
class FranchiseComment(models.Model):
    id = models.AutoField(primary_key = True)
    avatar = models.ImageField(verbose_name = 'Аватар коментирующего')
    fullname = models.CharField(max_length = 61, null = True, verbose_name = 'Имя и фамилия коментирующего')
    comment = models.TextField(max_length = 6000, verbose_name = 'Отзыв/Коментарий')
    active = models.CharField(max_length = 32, verbose_name = 'Активный коментарий')
    city = models.ForeignKey('Cities', verbose_name = 'Город коментирующего')

# Франшиза
class FranchiseAmount(models.Model):
    id = models.AutoField(primary_key = True)
    amount = models.IntegerField(verbose_name = 'Сумма')

class FranchiseCounter(models.Model):
    id = models.AutoField(primary_key = True)
    top_text = models.CharField(max_length = 32, verbose_name = 'Текст вверху')
    counter = models.IntegerField(verbose_name = 'Значение счётчика')
    bottom_text = models.CharField(max_length = 32, verbose_name = 'Текст вверху')

class FranchiseAdv(models.Model):
    id = models.AutoField(primary_key = True)
    text = models.TextField(max_length = 9000, verbose_name = 'Текст')
    bottom_text = models.CharField(max_length = 33, verbose_name = 'Описание')

class FranchiseItemAdv(models.Model):
    id = models.AutoField(primary_key = True)
    item_name = models.CharField(max_length = 90, verbose_name = 'Заголовок')
    item_text = models.CharField(max_length = 255, verbose_name = 'Текст')

class FranchiseItemAdvTwo(models.Model):
    id = models.AutoField(primary_key = True)
    item_name = models.CharField(max_length = 90, verbose_name = 'Заголовок')
    item_text = models.TextField(max_length = 90000, verbose_name = 'Текст')
    active = models.CharField(max_length = 32, verbose_name = 'Активный пункт')
    image = models.ImageField(verbose_name = 'Изображение')

class FranchiseItemPartner(models.Model):
    id = models.AutoField(primary_key = True)
    text = models.CharField(max_length = 66)
    amount = models.IntegerField()
    identity = models.CharField(max_length = 1000)

class FranchiseItemPartnerText(models.Model):
    id = models.AutoField(primary_key = True)
    text = models.TextField(max_length = 90000)

class FranchiseItemRequest(models.Model):
    id = models.AutoField(primary_key = True)
    item_name = models.CharField(max_length = 90)
    text = models.TextField(max_length = 9000)
    active = models.CharField(max_length = 32)
    image = models.ImageField()

class FranchiseItemRequestStagest(models.Model):
    id = models.AutoField(primary_key = True)
    name_stages = models.CharField(max_length = 90)
    text_stages = models.TextField(max_length = 9000)

class FranchiseItemFAQ(models.Model):
    id = models.AutoField(primary_key = True)
    item_name = models.CharField(max_length = 255)
    item_text = models.TextField(max_length = 9000)
    active = models.CharField(max_length = 32)

class FranchiseAbout(models.Model):
    id = models.AutoField(primary_key = True)
    about = models.TextField(max_length = 9000)
    image_avatar = models.ImageField()
    name = models.CharField(max_length = 90)
    description = models.CharField(max_length = 255)

class FranchiseHeader(models.Model):
    id = models.AutoField(primary_key = True)
    header_image = models.ImageField()

# Производство
class Company(models.Model):
    id = models.AutoField(primary_key = True)
    header_image = models.ImageField(verbose_name = 'Изображение страницы')
    text_caption = models.CharField(max_length = 322, verbose_name = 'Заголовок страницы')
    text_additional_caption = models.CharField(max_length = 900, verbose_name = 'Подзаголовок страницы')
    text_about_additional_caption = models.TextField(max_length = 9500, verbose_name = 'Описание подзаголовка страницы')
    text_about_us = models.TextField(max_length = 10000, verbose_name = 'О нас')
    text_company = models.ManyToManyField('CompanyText', verbose_name = 'Преимущества')
    image_technology = models.ImageField(verbose_name = 'Изображение этапов')
    image_company = models.ManyToManyField('CompanyImages', verbose_name = 'Изображения')

# Текст производства
class CompanyText(models.Model):
    id = models.AutoField(primary_key = True)
    text = models.CharField(max_length = 90, verbose_name = 'Текст')
    description = models.TextField(max_length = 9000, verbose_name = 'Описание')

class CompanyImages(models.Model):
    id = models.AutoField(primary_key = True)
    image = models.ImageField(verbose_name = 'Изображение')

