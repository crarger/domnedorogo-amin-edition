# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0114_auto_20180123_1133'),
    ]

    operations = [
        migrations.CreateModel(
            name='MainSlider',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('slide', models.ImageField(verbose_name='Изображение слайда', upload_to='')),
                ('text', models.CharField(verbose_name='Текст слайда', max_length=81)),
            ],
            options={
                'verbose_name_plural': 'Первый слайдер',
            },
        ),
        migrations.DeleteModel(
            name='MainSliders',
        ),
    ]
