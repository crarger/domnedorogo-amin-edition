# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-11-09 10:09
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0003_submenu'),
    ]

    operations = [
        migrations.CreateModel(
            name='StartCounter',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=32)),
                ('count', models.IntegerField()),
            ],
        ),
        migrations.RemoveField(
            model_name='submenu',
            name='menu',
        ),
        migrations.DeleteModel(
            name='Menu',
        ),
        migrations.DeleteModel(
            name='Submenu',
        ),
    ]
