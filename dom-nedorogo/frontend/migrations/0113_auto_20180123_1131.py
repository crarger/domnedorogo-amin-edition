# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0112_auto_20180123_1129'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='MainSlider',
            new_name='MainSliders',
        ),
    ]
