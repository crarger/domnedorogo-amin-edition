# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-12-05 09:05
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0087_auto_20171205_1202'),
    ]

    operations = [
        migrations.AlterField(
            model_name='franchisecomment',
            name='fullname',
            field=models.CharField(max_length=62, null=True, verbose_name='Имя Фамилия коментирующего'),
        ),
    ]
