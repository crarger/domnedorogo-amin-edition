# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0119_auto_20180123_1320'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contacts',
            name='lat',
            field=models.CharField(verbose_name='Ширина ', max_length=32, null=True),
        ),
        migrations.AlterField(
            model_name='contacts',
            name='lng',
            field=models.CharField(verbose_name='Долгота ', max_length=32, null=True),
        ),
        migrations.AlterField(
            model_name='contacts',
            name='zoom',
            field=models.IntegerField(verbose_name='Масштаб ', null=True),
        ),
    ]
