# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-12-06 11:46
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0094_auto_20171206_1444'),
    ]

    operations = [
        migrations.AlterField(
            model_name='franchiseitemadvtwo',
            name='item_text',
            field=models.TextField(max_length=90000, verbose_name='Текст'),
        ),
    ]
