# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0115_auto_20180123_1134'),
    ]

    operations = [
        migrations.DeleteModel(
            name='MainSlider',
        ),
    ]
