# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-11-28 11:28
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0059_headercontacts'),
    ]

    operations = [
        migrations.AddField(
            model_name='headercontacts',
            name='link',
            field=models.CharField(default=1, max_length=32),
            preserve_default=False,
        ),
    ]
