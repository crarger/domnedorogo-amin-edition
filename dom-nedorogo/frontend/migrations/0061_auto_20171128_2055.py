# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-11-28 17:55
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0060_headercontacts_link'),
    ]

    operations = [
        migrations.AddField(
            model_name='service',
            name='on_advantages',
            field=models.IntegerField(default=1, verbose_name='Блок преимуществ'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='service',
            name='on_images',
            field=models.IntegerField(default=1, verbose_name='Блок картинкок'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='service',
            name='on_images_tabs',
            field=models.IntegerField(default=1, verbose_name='Блок изображений с табами'),
            preserve_default=False,
        ),
    ]
