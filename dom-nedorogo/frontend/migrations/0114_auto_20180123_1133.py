# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0113_auto_20180123_1131'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mainsliders',
            name='text',
            field=models.CharField(verbose_name='Текст слайда', max_length=82),
        ),
    ]
