# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-11-28 08:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0053_counter_status'),
    ]

    operations = [
        migrations.CreateModel(
            name='ImageArticleTechnology',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('image', models.ImageField(upload_to='', verbose_name='Изображение которое вы увидите в статье')),
            ],
        ),
        migrations.AddField(
            model_name='articletechnology',
            name='image',
            field=models.ImageField(default=1, upload_to='', verbose_name='Изображение статьи'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='articletechnology',
            name='images',
            field=models.ManyToManyField(to='frontend.ImageArticleTechnology', verbose_name='Изображения статьи'),
        ),
    ]
