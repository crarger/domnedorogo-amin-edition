# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-11-29 11:46
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0067_auto_20171129_1315'),
    ]

    operations = [
        migrations.CreateModel(
            name='AdvatagesIcon',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('icon', models.ImageField(upload_to='')),
                ('caption', models.CharField(max_length=32)),
                ('description', models.CharField(max_length=32)),
            ],
        ),
    ]
