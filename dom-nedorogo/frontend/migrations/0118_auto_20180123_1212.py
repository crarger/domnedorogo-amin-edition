# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0117_mainslider'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mainslider',
            name='text',
            field=models.CharField(verbose_name='Текст слайда', max_length=255),
        ),
    ]
