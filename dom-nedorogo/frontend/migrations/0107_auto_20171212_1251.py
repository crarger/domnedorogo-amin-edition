# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-12-12 09:51
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0106_quest_imageslide'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='quest',
            name='imageSlide',
        ),
        migrations.AddField(
            model_name='service',
            name='imageSlide',
            field=models.ImageField(default=1, upload_to='', verbose_name='Изображение слайда'),
            preserve_default=False,
        ),
    ]
