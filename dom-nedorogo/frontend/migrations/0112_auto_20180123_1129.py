# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0111_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='MainSlider',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('slide', models.ImageField(verbose_name='Изображение слайда', upload_to='')),
                ('text', models.CharField(verbose_name='Текст слайда', max_length=80)),
            ],
            options={
                'verbose_name_plural': 'Первый слайдер',
            },
        ),
        migrations.RemoveField(
            model_name='projectdata',
            name='area',
        ),
        migrations.RemoveField(
            model_name='projectdata',
            name='floor',
        ),
        migrations.RemoveField(
            model_name='projectdata',
            name='height_floor',
        ),
        migrations.RemoveField(
            model_name='projectdata',
            name='rooms',
        ),
        migrations.RemoveField(
            model_name='projectlayout',
            name='all_area',
        ),
        migrations.RemoveField(
            model_name='projectlayout',
            name='amount_project',
        ),
        migrations.RemoveField(
            model_name='projectlayout',
            name='angle',
        ),
        migrations.RemoveField(
            model_name='projectlayout',
            name='area_roof',
        ),
        migrations.RemoveField(
            model_name='projectlayout',
            name='cubature',
        ),
        migrations.RemoveField(
            model_name='projectlayout',
            name='dev_area',
        ),
        migrations.RemoveField(
            model_name='projectlayout',
            name='garage',
        ),
        migrations.RemoveField(
            model_name='projectlayout',
            name='height',
        ),
        migrations.RemoveField(
            model_name='projectlayout',
            name='min_area_height',
        ),
        migrations.RemoveField(
            model_name='projectlayout',
            name='min_area_width',
        ),
        migrations.RemoveField(
            model_name='projectlayout',
            name='room_description',
        ),
        migrations.AddField(
            model_name='equipment',
            name='active',
            field=models.CharField(verbose_name='Активная комплектация', max_length=32, default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='equipment',
            name='amount',
            field=models.IntegerField(verbose_name='Цена комплектации', default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='oworks',
            name='status',
            field=models.IntegerField(verbose_name='Статус работы', default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='projectdata',
            name='height_one',
            field=models.CharField(verbose_name='Высота первого этажа', max_length=300, default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='projectdata',
            name='height_two',
            field=models.CharField(verbose_name='Высота второго этажа', max_length=300, default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='projectdata',
            name='living_space',
            field=models.CharField(verbose_name='Жилая площадь', max_length=255, default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='projectdata',
            name='number_bathrooms',
            field=models.IntegerField(verbose_name='Количество санузлов', default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='projectdata',
            name='number_floors',
            field=models.IntegerField(verbose_name='Количество этажей', default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='projectdata',
            name='number_rooms',
            field=models.CharField(verbose_name='Количество комнат', max_length=10, default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='projectdata',
            name='number_terraces',
            field=models.IntegerField(verbose_name='Количество террас', default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='projectdata',
            name='total_area',
            field=models.IntegerField(verbose_name='Общая площадь', default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='projectlayout',
            name='garage_area',
            field=models.IntegerField(verbose_name='Площадь гаража', default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='service',
            name='advantages',
            field=models.ManyToManyField(verbose_name='Преимущества', to='frontend.AdvatagesIcon'),
        ),
        migrations.AddField(
            model_name='service',
            name='imageSlide',
            field=models.ImageField(verbose_name='Изображение слайда', default=1, upload_to=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='service',
            name='title_advantages',
            field=models.CharField(verbose_name='Заголовок блока преимуществ', max_length=9000, default=1),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='advatagesicon',
            name='caption',
            field=models.CharField(max_length=64),
        ),
        migrations.AlterField(
            model_name='advatagesicon',
            name='description',
            field=models.CharField(max_length=255),
        ),
        migrations.AlterField(
            model_name='articleservice',
            name='article',
            field=models.TextField(max_length=99000),
        ),
        migrations.AlterField(
            model_name='contacts',
            name='phone',
            field=models.CharField(max_length=40),
        ),
        migrations.AlterField(
            model_name='projectdata',
            name='height',
            field=models.CharField(verbose_name='Высота дома', max_length=322),
        ),
        migrations.AlterField(
            model_name='projectdata',
            name='width',
            field=models.CharField(verbose_name='Ширина дома', max_length=322),
        ),
        migrations.AlterField(
            model_name='service',
            name='name',
            field=models.CharField(verbose_name='Имя услуги', max_length=255),
        ),
        migrations.AlterField(
            model_name='service',
            name='title_image_block',
            field=models.CharField(verbose_name='Заголовок блока с картинками', max_length=255),
        ),
    ]
