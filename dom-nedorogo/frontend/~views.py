﻿from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.conf import settings
from urllib.parse import *
from .models import *
from django.core.mail import send_mail
import requests, json
from django.core.mail import EmailMultiAlternatives
import operator
import datetime
from django.views.decorators.cache import cache_page

# Create your views here.
# Стартовая страница

#@cache_page()
def InstagramPosts(data = None):
    content = requests.get('https://www.instagram.com/domnedorogo/?__a=1').content
    data = json.loads(content)
    posts = []
    description_post = []
    counter = 0
    done = True
    count_post = 0

    while done:
        try:
            if (count_post != 8):
                result = data['user']['media']['nodes'][counter]
                description_post.append(result['caption'][0:120])
                posts.append(result['thumbnail_src'])
            else:
                done = False
            count_post += 1
        except:
            done = False

        counter += 1

    if (data == 'descrpt'):
        return description_post

    return posts

def SaveUser(request, city, ip):
    user_anonimus = Anonimus()
    user_anonimus.ip_addr = ip

    if (city):
        user_anonimus.city = unquote(unquote(city))
    else:
        user_anonimus.city = 'Нет города'

    try:
        user_anonimus.save()
    except:
        anonimus = Anonimus.objects.filter(ip_addr = ip).get()
        anonimus.city = unquote(unquote(city))
        anonimus.save()

    # Поиск городов
    load_city = Cities.objects.filter(name = unquote(unquote(city))).get()

    c = Cities.objects.all()

    list_result = []
    list_result.append({'city': load_city.name})

    return HttpResponse(json.dumps(list_result), content_type='application/json')

def GetCityUser(request, ip):

    # Поиск городов
    anonimus = Anonimus.objects.filter(ip_addr = ip).get()
    load_city = Cities.objects.filter(name = anonimus.city).get()

    list_result = []
    list_result.append({'city': load_city.name})

    return HttpResponse(json.dumps(list_result), content_type='application/json')

def home(request):

    ip = requests.get('https://api.ipify.org/').content

    try:
        menu = Menu.objects.all()
        services = Service.objects.all()
        cities = Cities.objects.all()
        counter = Counter.objects.all()
        years = Years.objects.all()
        header_contacts = HeaderContacts.objects.all()
        advantages = AdvatagesIcon.objects.all()
        settings = Settings.objects.get(pk = 1)
        partners = Partners.objects.all()

        context = {
            'title': '«ДомНедорого» – строительство домов по всей России',
            'class': 'page-index',
            'menu': menu,
            'services': services,
            'cities': cities,
            'counter': counter,
            'years': years,
            'head_contacts': header_contacts,
            'inst_images': InstagramPosts(),
            'inst_caption': InstagramPosts(data = 'descrpt'),
            'advantages': advantages,
            'settings': settings,
            'partners': partners,
            'ip': ip,
            'year': str(datetime.datetime.now()).split('-')[0],
        }
    except:
        context = {}


    return render(request, 'frontend/page-start.html', context)

# Страница наши работы
def works(request):

    menu = Menu.objects.all()
    services = Service.objects.all()
    works = OWorks.objects.all()
    cities = Cities.objects.all()
    header_contacts = HeaderContacts.objects.all()

    context = {
        'title': 'Наши работы – «ДомНедорого»',
        'class': 'pages works-page',
        'menu': menu,
        'services': services,
        'works': works,
        'cities': cities,
        'head_contacts': header_contacts,
        'year': str(datetime.datetime.now()).split('-')[0],
    }

    return render(request, 'frontend/page-works.html', context)

# Страница отдельной работы
def work(request, id = None):

    images_work = None
    menu = Menu.objects.all()
    services = Service.objects.all()
    cities = Cities.objects.all()
    header_contacts = HeaderContacts.objects.all()


    if (id != None):
        images_work = OWorks.objects.get(pk = id)
    else:
        try:
            images_work = OWorks.objects.all()[0]
        except:
            images_work = None

    context = {
        'title': 'Строительство компанией «ДомНедорого»',
        'class': 'pages etapes-page',
        'menu': menu,
        'services': services,
        'images_work': images_work,
        'cities': cities,
        'head_contacts': header_contacts,
        'year': str(datetime.datetime.now()).split('-')[0],
    }

    return render(request, 'frontend/stages.html', context)

# Страница проектов
def projects(request, sort = 1):

    try:
        amount_1 = request.GET['a1']
        amount_2 = request.GET['a2']
    except:
        amount_1 = None
        amount_2 = None

    try:
        area_1 = request.GET['area_1']
        area_2 = request.GET['area_2']
    except:
        area_1 = None
        area_2 = None

    menu = Menu.objects.all()
    services = Service.objects.all()

    projects = Project.objects.all()

    cities = Cities.objects.all()
    header_contacts = HeaderContacts.objects.all()

    if (int(sort) == 2):
        projects = reversed(sorted(projects, key=operator.attrgetter('amount')))
    else:
        projects = sorted(projects, key=operator.attrgetter('amount'))

    if ((amount_1 != None) and (amount_2 != None)):
        projects = Project.objects.all().filter(amount__gt = unquote(unquote(amount_1))).exclude(amount__gt = unquote(unquote(amount_2)))

    if ((area_1 != None) and (area_2 != None)):
        projects = Project.objects.all().filter(data__total_area__gt = area_1).exclude(data__total_area__gt = area_2)

    context = {
        'title': 'Каталог проектов строительной компании «ДомНедорого»',
        'class': 'pages catalog-page',
        'menu': menu,
        'services': services,
        'projects': projects,
        'min_area': 10,
        'max_area': 1000,
        'cities': cities,
        'message': 0,
        #'pages': pages,
        'head_contacts': header_contacts,
        'year': str(datetime.datetime.now()).split('-')[0],
    }

    return render(request, 'frontend/catalog.html', context)

# Страница отдельного проекта
def project_detail(request, id):

    menu = Menu.objects.all()
    services = Service.objects.all()

    project = Project.objects.get(pk = id)

    works = OWorks.objects.all()

    cities = Cities.objects.all()
    header_contacts = HeaderContacts.objects.all()

    try:
        project_back = Project.objects.get(pk = int(id) - 1)
    except:
        project_back = None

    try:
        project_next = Project.objects.get(pk = int(id) + 1)
    except:
        project_next = None

    context = {
        'title': 'Проект',
        'class': 'pages body-page-detail',
        'menu': menu,
        'services': services,
        'project': project,
        'works': works,
        'cities': cities,
        'head_contacts': header_contacts,
        'project_back': project_back,
        'project_next': project_next,
        'year': str(datetime.datetime.now()).split('-')[0],
    }

    return render(request, 'frontend/page-project-detail.html', context)

# Страница котакты
def contacts(request):

    menu = Menu.objects.all()
    services = Service.objects.all()

    icons = ContactsSocialNetwork.objects.all()
    conatcts = Contacts.objects.all()

    cities = Cities.objects.all()
    header_contacts = HeaderContacts.objects.all()

    context = {
        'title': 'Контакты строительной компании «ДомНедорого»',
        'class': 'pages contact-page',
        'menu': menu,
        'services': services,
        'icons': icons,
        'contacts': conatcts,
        'cities': cities,
        'head_contacts': header_contacts,
        'year': str(datetime.datetime.now()).split('-')[0],
    }

    return render(request, 'frontend/contacts.html', context)

# Страница часто задаваемых вопросов
def question(request, id = None):

    article_question = None

    menu = Menu.objects.all()
    services = Service.objects.all()
    category_quest = CategoryQuest.objects.all()

    cities = Cities.objects.all()
    header_contacts = HeaderContacts.objects.all()

    if (id != None):
        article_question = Quest.objects.get(pk = id)
    else:
        try:
            article_question = Quest.objects.all()[0]


        except:
            article_question = None

    context = {
        'title': 'Частозадаваемые вопросы',
        'class': 'pages',
        'menu': menu,
        'services': services,
        'questions': category_quest,
        'article_question': article_question,
        'cities': cities,
        'head_contacts': header_contacts,
        'year': str(datetime.datetime.now()).split('-')[0],
    }

    return render(request, 'frontend/page-questions.html', context)

# Страница технологий
def technology(request, id = None):

    article_technology = None

    menu = Menu.objects.all()
    services = Service.objects.all()
    category_technology = CategoryTechnology.objects.all()

    cities = Cities.objects.all()

    article_quest = Quest.objects.all()
    header_contacts = HeaderContacts.objects.all()

    if (id != None):
        article_technology = Technology.objects.get(pk = id)
    else:
        try:
            article_technology = Technology.objects.all()[0]
        except:
            article_technology = None

    context = {
        'title': 'Статьи о технологиях производства строительной компании «ДомНедорого»',
        'class': 'pages',
        'menu': menu,
        'services': services,
        'technology': category_technology,
        'article_technology': article_technology,
        'cities': cities,
        'head_contacts': header_contacts,
        'year': str(datetime.datetime.now()).split('-')[0],
    }

    return render(request, 'frontend/page-technology.html', context)

# Страница услуг
def service(request, id = None):

    services = None

    menu = Menu.objects.all()
    services = Service.objects.all()

    cities = Cities.objects.all()
    header_contacts = HeaderContacts.objects.all()

    if (id != None):
        service = Service.objects.get(pk = id)
    else:
        try:
            service = Service.objects.all()[0] # Создание
        except:
            service = None

    context = {
        'title': 'Услуги',
        'class': 'pages services-page',
        'menu': menu,
        'services': services,
        'service': service,
        'cities': cities,
        'head_contacts': header_contacts,
        'year': str(datetime.datetime.now()).split('-')[0],
    }

    return render(request, 'frontend/services.html', context)

# Страница о проекте
def about(request):

    menu = Menu.objects.all()
    services = Service.objects.all()
    cities = Cities.objects.all()
    header_contacts = HeaderContacts.objects.all()
    about = About.objects.get(pk = 1)

    context = {
        'title': 'О компании «ДомНедорого»',
        'class': 'pages about-page',
        'menu': menu,
        'services': services,
        'cities': cities,
        'head_contacts': header_contacts,
        'about': about,
        'year': str(datetime.datetime.now()).split('-')[0],
    }

    return render(request, 'frontend/about.html', context)

# Страница франшиза
def franchise(request):

    menu = Menu.objects.all()
    header_contacts = HeaderContacts.objects.all()
    services = Service.objects.all()
    present = Present.objects.get(pk = 1)
    cities = Cities.objects.all()
    franchise_comment = FranchiseComment.objects.all()
    franchise_amount = FranchiseAmount.objects.get(pk = 1)
    franchise_counter = FranchiseCounter.objects.all()
    franchise_adv = FranchiseAdv.objects.get(pk = 1)
    franchise_item_adv = FranchiseItemAdv.objects.all()
    franchise_item_adv_two = FranchiseItemAdvTwo.objects.all()
    franchise_item_partner = FranchiseItemPartner.objects.all()
    franchise_item_partner_text = FranchiseItemPartnerText.objects.get(pk = 1)
    franchise_item_request = FranchiseItemRequest.objects.all()
    franchise_item_request_stagest = FranchiseItemRequestStagest.objects.all()
    franchise_item_faq = FranchiseItemFAQ
    franchise_about = FranchiseAbout.objects.get(pk = 1)
    franchise_header_image = FranchiseHeader.objects.get(pk = 1)



    context = {
        'title': 'Франшиза строительной компании «ДомНедорого»',
        'class': 'franchise',
        'menu': menu,
        'services': services,
        'present': present,
        'cities': cities,
        'head_contacts': header_contacts,
        'franchise_amount': franchise_amount,
        'franchise_counter': franchise_counter,
        'franchise_adv': franchise_adv,
        'franchise_item_adv': franchise_item_adv,
        'franchise_item_adv_two': franchise_item_adv_two,
        'franchise_item_partner': franchise_item_partner,
        'franchise_item_partner_text': franchise_item_partner_text,
        'franchise_item_request': franchise_item_request,
        'franchise_item_request_stagest': franchise_item_request_stagest,
        'franchise_comment': franchise_comment,
        'franchise_item_faq': franchise_item_faq.objects.all(),
        'fif': franchise_item_faq.objects.get(pk = 1),
        'franchise_about': franchise_about,
        'franchise_header_image': franchise_header_image,
        'year': str(datetime.datetime.now()).split('-')[0],
    }

    return render(request, 'frontend/franchise.html', context)

# Страница документации
@login_required
def doc(request):

    menu = Menu.objects.all()
    services = Service.objects.all()
    cities = Cities.objects.all()

    context = {
        'title': 'Документация',
        'class': 'pages about-page',
        'menu': menu,
        'services': services,
        'cities': cities,
        'year': str(datetime.datetime.now()).split('-')[0],
    }

    return render(request, 'frontend/doc.html', context)

def company(request):

    menu = Menu.objects.all()
    services = Service.objects.all()
    cities = Cities.objects.all()
    header_contacts = HeaderContacts.objects.all()
    company = Company.objects.get(pk = 1)

    context = {
        'title': 'Производство каркасных домокомплектов – «ДомНедорого»',
        'class': 'pages about-page franchise',
        'menu': menu,
        'services': services,
        'cities': cities,
        'head_contacts': header_contacts,
        'company': company,
        'year': str(datetime.datetime.now()).split('-')[0],
    }

    return render(request, 'frontend/company.html', context)


def GetEquipments(request, equ_id):

    equipment = Equipment.objects.get(pk = equ_id)

    return HttpResponse(equipment.amount)

def FranchiseRequest(request):

    name = request.POST['full-name']
    phone = request.POST['num-phone']
    email = request.POST['email']
    set_city = request.POST['city']
    req_email = request.POST['request']

    req = 'Город: ' + set_city + '\r\n'
    req += 'Имя: ' + name + '\r\n'
    req += 'Телефон: ' + phone + '\r\n'
    req += 'E-mail: ' + email + '\r\n'
    req += 'Текст письма: ' + req_email + '\r\n'

    send_mail('Дом недорого - Франшиза', req, settings.EMAIL_HOST_USER, ['domnedorogo35@yandex.ru'])

    return redirect('/')

def CompanyRequest(request):

    name = request.POST['full-name']
    phone = request.POST['number-phone']
    email = request.POST['email']
    req_email = request.POST['request']

    req = 'Имя: ' + name + '\r\n'
    req += 'Телефон: ' + phone + '\r\n'
    req += 'E-mail: ' + email + '\r\n'
    req += 'Текст письма: ' + req_email + '\r\n'

    send_mail('Дом недорого - Оценка дома', req, settings.EMAIL_HOST_USER, ['domnedorogo35@yandex.ru'])

    return redirect('/')

def HomeRequest(request):

    name = request.POST['full-name']
    phone = request.POST['num-phone']
    city = request.POST['city']
    discount = request.POST['discount']

    req = 'Город: ' + city + '\r\n'
    req += 'Имя: ' + name + '\r\n'
    req += 'Телефон: ' + phone + '\r\n'
    req += 'Скидка: ' + discount + '%' + '\r\n'

    send_mail('Дом недорого - Дома', req, settings.EMAIL_HOST_USER, ['domnedorogo35@yandex.ru'])

    return redirect('/')

def GetComment(request, id):
    comment = FranchiseComment.objects.get(pk = id)

    array_comment = []
    array_comment.append({'avatar': comment.avatar})
    array_comment.append({'fullname': comment.fullname})
    array_comment.append({'city': comment.city})
    array_comment.append({'comment': comment.comment})

    return HttpResponse(array_comment)

def DownloadPresent(request):

    return redirect('/franchise/')

    name = request.POST['full-name']
    phone = request.POST['num-phone']
    email = request.POST['email']

    present_franchise = request.POST['present-franchise'];
    present_comerce = request.POST['present-comerce'];

    req = 'Имя: ' + name + '\r\n'
    req += 'Телефон: ' + phone + '\r\n'
    req += 'E-mail: ' + email + '\r\n'

    print(present_franchise)

    send_mail('Дом недорого - Скачивание презентации', req, settings.EMAIL_HOST_USER, ['domnedorogo35@yandex.ru'])


    if (present_franchise == 1):
        message = EmailMultiAlternatives('Презентация ДомНедорого', 'Мы отправили вам презетацию.', settings.EMAIL_HOST_USER, [email])
        message.mixed_subtype = 'related'
        message.attach_file('/media/Report.pdf', read_pdf_contents(), 'application/pdf')
        message.send()

        return redirect('/franchise/')

    if (present_comerce == 1):
        send_mail('Дом недорого - Ваша презентация', req, settings.EMAIL_HOST_USER, [email])

    if (present_franchise == 1 and present_comerce == 1):
        message = EmailMultiAlternatives('Презентация ДомНедорого', 'Мы отправили вам презетацию.', settings.EMAIL_HOST_USER, [email])
        message.mixed_subtype = 'related'
        message.attach_file('/media/Report.pdf', read_pdf_contents(), 'application/pdf')
        message.send()

def page_not_found(request):

    context = {
        'title': 'Страница не найдена',
        'class': 'pages system-page',
    }

    return render(request, '404.html', context)

def project_send_mail(request):

    if (request.method == 'POST'):
        project_id = request.POST['project-id']
        equipment_id = request.POST['equipment-id']
        fullname = request.POST['full-name']
        phone = request.POST['num-phone']
        email = request.POST['email']
        city = request.POST['city']

        if (project_id and equipment_id and fullname and phone and email and city):
            project = Project.objects.get(pk = int(project_id))
            equipment = Equipment.objects.get(pk = int(equipment_id))

            req = 'Проект: Дом ' + project.name + '\r\n'
            req += 'Комплектация: ' + equipment.name + '\r\n'
            req += 'Город: ' + city + '\r\n'
            req += 'Имя: ' + fullname + '\r\n'
            req += 'Телефон: ' + phone + '\r\n'
            req += 'E-mail: ' + email + '\r\n'
            send_mail('Дом недорого - Заказ проекта', req, settings.EMAIL_HOST_USER, ['domnedorogo35@yandex.ru'])
            return redirect('/project/' + str(project_id))    
        else:
            return redirect('/project/' + str(project_id))

    