from django.contrib import admin

# Register your models here.
from django.contrib import admin
from .models import *

admin.site.register(Menu)
admin.site.register(Submenu)
admin.site.register(Service)
admin.site.register(ArticleService)
admin.site.register(NameCounter)
admin.site.register(Counter)
admin.site.register(Cities)
admin.site.register(CategoryQuest)
admin.site.register(Quest)
admin.site.register(ArticleQuest)
admin.site.register(CategoryTechnology)
admin.site.register(Technology)
admin.site.register(ArticleTechnology)
admin.site.register(Tags)

admin.site.register(OWorks)

admin.site.register(SocialNetwork)
admin.site.register(ContactsSocialNetwork)
admin.site.register(Contacts)

admin.site.register(ImageWork)

admin.site.register(Project)
admin.site.register(ImageProject)
admin.site.register(Equipment)
admin.site.register(ProjectData)
admin.site.register(ProjectLayout)

admin.site.register(Years)
admin.site.register(Present)

admin.site.register(ImageArticleTechnology)
admin.site.register(ImageArticleQuest)

admin.site.register(HeaderContacts)
admin.site.register(ImageService)
admin.site.register(Anonimus)
admin.site.register(AdvatagesIcon)
admin.site.register(Settings)
admin.site.register(Stages)
admin.site.register(Partners)
admin.site.register(About)
admin.site.register(FranchiseComment)

admin.site.register(FranchiseAmount)
admin.site.register(FranchiseCounter)
admin.site.register(FranchiseAdv)
admin.site.register(FranchiseItemAdv)
admin.site.register(FranchiseItemAdvTwo)
admin.site.register(FranchiseItemPartner)
admin.site.register(FranchiseItemPartnerText)
admin.site.register(FranchiseItemRequest)
admin.site.register(FranchiseItemRequestStagest)
admin.site.register(FranchiseItemFAQ)
admin.site.register(FranchiseAbout)
admin.site.register(FranchiseHeader)

admin.site.register(Company)
admin.site.register(CompanyText)
admin.site.register(CompanyImages)

admin.site.register(MainSlider)
admin.site.register(BottomSlider)