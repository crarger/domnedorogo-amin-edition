var gulp = require('gulp'), // Подключаем Gulp
    // sass = require('gulp-sass'), //Подключаем Sass пакет,
    cssnano = require('gulp-cssnano'), // Подключаем пакет для минификации CSS
    rigger = require('gulp-rigger'),
    rename = require('gulp-rename'), // Подключаем библиотеку для переименования файлов
    // concat = require('gulp-concat'), // Подключаем gulp-concat (для конкатенации файлов)
    htmlmin = require('gulp-htmlmin'), // Подключаем gulp-htmlmin (для минификации html)
    uglify = require('gulp-uglifyjs'), // Подключаем gulp-uglifyjs (для сжатия JS)
    imagemin = require('gulp-imagemin'), // Подключаем библиотеку для работы с изображениями
    pngquant = require('imagemin-pngquant'), // Подключаем библиотеку для работы с png
    // autoprefixer = require('gulp-autoprefixer'), // Подключаем autoprefixer
    // postcss = require('gulp-postcss'),
    // uncss = require('postcss-uncss'),
    fontmin = require('gulp-fontmin'),
    svgmin = require('gulp-svgmin'); // Подключаем Минификатор svg



// Задаем пути
var path = {
    build: { //Тут мы укажем куда складывать готовые после сборки файлы
        html: 'templates/',
        style: 'static/frontend/css/',
        img: 'static/frontend/img/',
        media: 'media/',
        svg: 'static/frontend/svg/',
        fonts: 'static/frontend/fonts/'
    },
    sources: { //Пути откуда брать исходники
        html: 'sources/templates/**/*.html',
        style: 'sources/css/**/*.css',
        img: 'sources/img/**/*.*',
        media: 'media/**/*.*',
        fonts: 'sources/fonts/**/*.*',
        svg: 'sources/svg/**/*.svg'
    }
};




//минифицаруем svg
gulp.task('min-svg', function () {
    return gulp.src(path.sources.svg)
        .pipe(svgmin())
        .pipe(gulp.dest(path.build.svg));
});




//минимизируем свои стили и выносим в билд
gulp.task('min-css', function () {
    return gulp.src(path.sources.style) // Выбираем файл для минификации
        .pipe(rigger())
        .pipe(cssnano()) // Сжимаем
        .pipe(rename({suffix: '.min'})) // Добавляем суффикс .min
        .pipe(gulp.dest(path.build.style)); // Выгружаем в папку app/css
});




gulp.task('fontmin', function() {
    return gulp.src(path.sources.fonts)
        .pipe(fontmin())
        .pipe(gulp.dest(path.build.fonts));
});




gulp.task('min-img', function () {
    return gulp.src(path.sources.img) // Берем все изображения из app
        .pipe(imagemin({ // Сжимаем их с наилучшими настройками
            interlaced: true,
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest(path.build.img)); // Выгружаем на продакшен
});



gulp.task('min-media', function () {
    return gulp.src(path.sources.media) // Берем все изображения из app
        .pipe(imagemin({ // Сжимаем их с наилучшими настройками
            interlaced: true,
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest(path.build.media)); // Выгружаем на продакшен
});




gulp.task('min-html', function () {
    return gulp.src(path.sources.html)
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest(path.build.html));
});




//минифицируем и переносим js файлы
gulp.task('min-js', function () {
    return gulp.src([ // Берем все необходимые библиотеки
        'sources/js/map.js',
        'sources/js/script.js',
        'sources/js/calc.js',
        'sources/js/equ_amount.js',
        'sources/js/franchise-script.js',
        'sources/js/nouislider.js',
        'sources/js/owl-init.js',
        // 'sources/js/page-start-script.js',
        'sources/js/project.js',
        'sources/js/save_user.js',
        'sources/js/script-range.js',
        'sources/js/script-slider.js',
        'sources/js/services-script.js'
    ])
        .pipe(uglify()) // Сжимаем JS файл
        .pipe(rename({suffix: '.min'})) // Добавляем суффикс .min
        .pipe(gulp.dest('static/frontend/js'))
});



// //настраиваем сервер
// gulp.task('browser-sync', function () { // Создаем таск browser-sync
//     browserSync({ // Выполняем browserSync
//         server: { // Определяем параметры сервера
//             baseDir: 'dist' // Директория для сервера - app
//         },
//         notify: false // Отключаем уведомления
//     });
// });


// //прогоняем css через автопрефиксер
// gulp.task('autoprefix', () =>
// gulp.src('static/css/**/*.css')
//     .pipe(autoprefixer({
//         browsers: ['last 7 versions'],
//         cascade: false
//     }))
//     .pipe(gulp.dest('dist/css'))
// );



//билд
gulp.task('bild', ['min-js', 'min-css', 'min-svg', 'min-img', 'fontmin'], function () {
});


